# Tabel of contents
- data generation.ipynb: the code that contains the Holstein dimer and the method of exact diagonalization. It's also used to generate all the plots in the development report.
- *.npy data contained in the plot with same name
- exept for z*.npy:
    -z.npy is Delta for M=20
    -z3.npy is Delta for M=25
    -z4.npy is Delta for M=30
    -z5.npy is Delta for M=35
    all are on a grid of an interval (omega_0)x(gamma) =(0.01,2)x(-2,2)
