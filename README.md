# Bachelor thesis Thermodynamics of Holstein chains from quantum-classical hybrid methods

## Author:
Mohamad Al Farhan
## Supervisors:
Prof. Dr. Fabian Heidrich-Meisner  
David Jansen 
## Required packages:
- numpy
- matplotlib
- scipy
- functools
- copy
- vegas
- joblib
## The project includes:

### Folders:
- Bachelor's thesis: Folder that containts code, data and plots of the Bachelor's thesis "Thermodynamics of Holstein chains from
quantum-classical hybrid methods". 
- Einführungsbericht: Folder with used code for the development report: "Einführung ins wissenschaftliche Arbeiten: Numerical analysis of the Holstein model using the method of exact diagonalization"
It also includes code used to generate the plots and the data of the plots used in the report. 
- legacy: old test code and data not used any where
